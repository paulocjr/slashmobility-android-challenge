package com.slashmobility.seleccion.paulo.cesar.view.main

import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.slashmobility.seleccion.paulo.cesar.R
import com.slashmobility.seleccion.paulo.cesar.databinding.ActivityMainBinding
import com.slashmobility.seleccion.paulo.cesar.repository.onFailure
import com.slashmobility.seleccion.paulo.cesar.repository.onSuccess
import com.slashmobility.seleccion.paulo.cesar.view.base.BaseActivity
import com.slashmobility.seleccion.paulo.cesar.view.list.ListActivity
import com.slashmobility.seleccion.paulo.cesar.viewmodel.main.MainViewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(R.layout.activity_main) {

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun initBinding() {
        getBinding().btnCallService.setOnClickListener {
            callService()
        }

        getBinding().btnListActivity.setOnClickListener {
            goToListActivity()
        }
    }

    /**
     * Call service
     */
    private fun callService() {
        getViewModel().requestService().onSuccess {
            getViewModel().responseValue.set(it.origin)
            getViewModel().reversedValue.set(it.origin.reversed())
        }.onFailure {
            Toast.makeText(this, getString(R.string.server_error), Toast.LENGTH_SHORT).show()
        }.subscribe()
    }

    /**
     * Action to go to list activity
     */
    private fun goToListActivity() {
        startActivity(Intent(this, ListActivity::class.java))
    }
}