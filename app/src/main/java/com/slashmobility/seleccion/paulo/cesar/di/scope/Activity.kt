package com.slashmobility.seleccion.paulo.cesar.di.scope

import javax.inject.Scope

/**
 * Created by pcamilo on 17/06/2020.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Activity