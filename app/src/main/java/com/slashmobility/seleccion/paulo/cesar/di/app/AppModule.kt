package com.slashmobility.seleccion.paulo.cesar.di.app

import android.app.Application
import android.content.Context
import com.slashmobility.seleccion.paulo.cesar.service.APIClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pcamilo on 17/06/2020.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideApiClient() = APIClient()
}