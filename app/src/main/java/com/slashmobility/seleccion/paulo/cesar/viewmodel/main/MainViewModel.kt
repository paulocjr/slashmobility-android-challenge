package com.slashmobility.seleccion.paulo.cesar.viewmodel.main

import androidx.databinding.ObservableField
import com.slashmobility.seleccion.paulo.cesar.models.RequestModel
import com.slashmobility.seleccion.paulo.cesar.repository.ObservableBaseData
import com.slashmobility.seleccion.paulo.cesar.repository.main.RequestSampleRepository
import com.slashmobility.seleccion.paulo.cesar.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * Created by pcamilo on 17/06/2020.
 */
class MainViewModel @Inject constructor(private val repository: RequestSampleRepository) : BaseViewModel() {

    var responseValue = ObservableField("")
    var reversedValue = ObservableField("")

    fun requestService() : ObservableBaseData<RequestModel> =
            getAsyncData(
                    repository.requestService()
            )
}