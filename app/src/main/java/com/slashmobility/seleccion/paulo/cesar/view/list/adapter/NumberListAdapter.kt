package com.slashmobility.seleccion.paulo.cesar.view.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.slashmobility.seleccion.paulo.cesar.R
import com.slashmobility.seleccion.paulo.cesar.databinding.LayoutItemNumberBinding
import com.slashmobility.seleccion.paulo.cesar.viewmodel.list.ListViewModel

/**
 * Created by pcamilo on 18/06/2020.
 */
class NumberListAdapter(private val viewModel: ListViewModel) :
        RecyclerView.Adapter<NumberListAdapter.NumberListViewHolder>() {

    var data = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberListViewHolder {
        val binding = LayoutItemNumberBinding.inflate(LayoutInflater.from(parent.context))
        return NumberListViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: NumberListViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class NumberListViewHolder(private val binding: LayoutItemNumberBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Int) {
            binding.item = item

            //Remove item clicked
            binding.rootView.setOnClickListener {
                removeItemClicked()
            }

            binding.executePendingBindings()
            changeBackgroundColor()
        }

        private fun changeBackgroundColor() {
            val mainContent = binding.rootView
            if (adapterPosition % 2 == 0) {
                mainContent.setBackgroundColor(ContextCompat.getColor(binding.rootView.context, R.color.colorPrimary))
            } else {
                mainContent.setBackgroundColor(ContextCompat.getColor(binding.rootView.context, R.color.colorPrimaryDark))
            }
        }

        /**
         * Remove item clicked
         */
        private fun removeItemClicked() {
            if (adapterPosition != (-1)) {
                data.removeAt(adapterPosition)

                if (data.isNullOrEmpty()) {
                    viewModel.isEmptyList.set(true)
                }

                notifyDataSetChanged()
                changeBackgroundColor()
            }
        }
    }

    /**
     * Add new items in the List
     */
    fun addItem(value: List<Int>) {
        this.data.addAll(value)
        notifyDataSetChanged()
    }

    /**
     * Remove all items
     */
    fun removeAllItems() {
        if (data.isNotEmpty()) {
            data.clear()
            viewModel.isEmptyList.set(true)
        } else {
            viewModel.isEmptyList.set(false)
        }
    }

    /**
     * Sort ascending list
     */
    fun sortAscendingList() {
        data.sort()
        notifyDataSetChanged()
    }
}