package com.slashmobility.seleccion.paulo.cesar.view.list

import android.widget.Toast
import com.slashmobility.seleccion.paulo.cesar.R
import com.slashmobility.seleccion.paulo.cesar.databinding.ActivityListBinding
import com.slashmobility.seleccion.paulo.cesar.utils.Utils
import com.slashmobility.seleccion.paulo.cesar.view.base.BaseActivity
import com.slashmobility.seleccion.paulo.cesar.viewmodel.list.ListViewModel

class ListActivity : BaseActivity<ActivityListBinding, ListViewModel>(R.layout.activity_list) {

    override fun getViewModelClass(): Class<ListViewModel>  = ListViewModel::class.java

    override fun initBinding() {
        listeners()
    }

    /**
     * Listeners available in this screens
     */
    private fun listeners() {
        // Add button
        getBinding().btnAdd.setOnClickListener {
            if (Utils.fieldIsEmpty(getBinding().etAddItem)) {
                Toast.makeText(this, getString(R.string.field_empty_message), Toast.LENGTH_SHORT).show()
            } else {
                getViewModel().addSortItem(getBinding().etAddItem.text.toString().toInt())
            }
        }

        // Remove all entries
        getBinding().btnRemoveAll.setOnClickListener {
            getViewModel().removeAllItems()
        }

        // Sort ascending
        getBinding().btnSortList.setOnClickListener {
            getViewModel().sortAscendingList()
        }

    }

}