package com.slashmobility.seleccion.paulo.cesar.models

import com.slashmobility.seleccion.paulo.cesar.service.models.RequestResponse

/**
 * Created by pcamilo on 17/06/2020.
 */

/**
 * Convert Header response to model
 */
fun RequestResponse.toModel() = RequestModel(
        origin = this.origin,
        url = this.url
)