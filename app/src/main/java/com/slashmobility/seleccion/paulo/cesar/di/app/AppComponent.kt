package com.slashmobility.seleccion.paulo.cesar.di.app

import android.app.Application
import com.slashmobility.seleccion.paulo.cesar.SplashMobilityApplication
import com.slashmobility.seleccion.paulo.cesar.di.builder.ActivityBuilder
import com.slashmobility.seleccion.paulo.cesar.di.builder.ViewModelBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by pcamilo on 17/06/2020.
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        ViewModelBuilder::class,
        ActivityBuilder::class,
        AndroidSupportInjectionModule::class]
)
interface AppComponent {

    fun inject(application: SplashMobilityApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}