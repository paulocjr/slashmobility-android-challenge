package com.slashmobility.seleccion.paulo.cesar.models

/**
 * Created by pcamilo on 17/06/2020.
 */
class RequestModel(
       val origin: String,
       val url: String)