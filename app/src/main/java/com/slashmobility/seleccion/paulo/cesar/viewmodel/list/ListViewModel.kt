package com.slashmobility.seleccion.paulo.cesar.viewmodel.list

import androidx.databinding.ObservableField
import com.slashmobility.seleccion.paulo.cesar.view.list.adapter.NumberListAdapter
import com.slashmobility.seleccion.paulo.cesar.viewmodel.BaseViewModel
import java.util.*
import javax.inject.Inject

/**
 * Created by pcamilo on 17/06/2020.
 */
class ListViewModel @Inject constructor() : BaseViewModel() {

    var isEmptyList = ObservableField(true)
    private var adapter: NumberListAdapter? = null

    /**
     * Remove all items in the list
     */
    fun removeAllItems(){
        adapter?.removeAllItems()
    }

    /**
     * Add sort item in the list
     */
    fun addSortItem(quantity: Int) {

        val listItem = arrayListOf<Int>()
        for (i in 1..quantity) {
            listItem.add(getRandomNumberInRange())
        }

        if (listItem.isNotEmpty()) {
            isEmptyList.set(false)
            adapter?.addItem(listItem.toList())
        }

    }

    /**
     * Generate random number using range
     */
    private fun getRandomNumberInRange(min: Int = 0, max: Int = 999): Int {
        val r = Random()
        return r.nextInt(max - min + 1) + min
    }

    /**
     * Sort ascending list
     */
    fun sortAscendingList() {
        adapter?.sortAscendingList()
    }

    /**
     * Set the adapter in XML
     */
    fun getAdapter(): NumberListAdapter? {
        if (adapter == null) {
            adapter = NumberListAdapter(this)
        }
        return adapter
    }
}