package com.slashmobility.seleccion.paulo.cesar.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by pcamilo on 17/06/2020.
 */
class RequestResponse (
        @SerializedName("origin") val origin: String,
        @SerializedName("url") val url: String)