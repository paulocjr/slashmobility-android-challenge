package com.slashmobility.seleccion.paulo.cesar.service

import com.slashmobility.seleccion.paulo.cesar.repository.ObservableResponse
import com.slashmobility.seleccion.paulo.cesar.service.models.RequestResponse
import retrofit2.http.GET

/**
 * Created by pcamilo on 17/06/2020.
 */
interface RequestSampleService {

    @GET("get")
    fun requestService() : ObservableResponse<RequestResponse>
}