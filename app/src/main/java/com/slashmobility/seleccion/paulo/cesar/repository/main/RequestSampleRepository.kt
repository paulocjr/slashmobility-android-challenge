package com.slashmobility.seleccion.paulo.cesar.repository.main

import com.slashmobility.seleccion.paulo.cesar.models.RequestModel
import com.slashmobility.seleccion.paulo.cesar.models.toModel
import com.slashmobility.seleccion.paulo.cesar.repository.ObservableBaseData
import com.slashmobility.seleccion.paulo.cesar.repository.mapToBaseData
import com.slashmobility.seleccion.paulo.cesar.repository.mapToBaseResponse
import com.slashmobility.seleccion.paulo.cesar.service.APIClient
import com.slashmobility.seleccion.paulo.cesar.service.RequestSampleService
import javax.inject.Inject

/**
 * Created by pcamilo on 17/06/2020.
 */
class RequestSampleRepository @Inject constructor (apiClient: APIClient) : IRequestRepository {

    private val mRequestSampleService: RequestSampleService = apiClient.getRetrofit().create(RequestSampleService::class.java)

    override fun requestService() : ObservableBaseData<RequestModel>  {
        return mRequestSampleService.requestService()
                .mapToBaseResponse()
                .mapToBaseData { it.toModel() }
    }
}