package com.slashmobility.seleccion.paulo.cesar.view.splash

import android.content.Intent
import android.os.Handler
import com.slashmobility.seleccion.paulo.cesar.R
import com.slashmobility.seleccion.paulo.cesar.databinding.ActivitySplashBinding
import com.slashmobility.seleccion.paulo.cesar.utils.Utils
import com.slashmobility.seleccion.paulo.cesar.view.base.BaseActivity
import com.slashmobility.seleccion.paulo.cesar.view.main.MainActivity
import com.slashmobility.seleccion.paulo.cesar.viewmodel.splash.SplashViewModel

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(R.layout.activity_splash) {

    override fun getViewModelClass(): Class<SplashViewModel> = SplashViewModel::class.java

    override fun initBinding() {
        Utils.blinkingAnimation(getBinding().ivSplash)
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            openMainActivity()
        }, 2500)
    }

    private fun openMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
        finish()
    }
}