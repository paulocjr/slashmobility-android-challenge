package com.slashmobility.seleccion.paulo.cesar.repository.main

import com.slashmobility.seleccion.paulo.cesar.models.RequestModel
import com.slashmobility.seleccion.paulo.cesar.repository.ObservableBaseData

/**
 * Created by pcamilo on 17/06/2020.
 */
interface IRequestRepository {

    /**
     * Get all information about the service requested
     */
    fun requestService() : ObservableBaseData<RequestModel>
}