package com.slashmobility.seleccion.paulo.cesar.viewmodel

import androidx.lifecycle.ViewModel
import com.slashmobility.seleccion.paulo.cesar.models.BaseData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by pcamilo on 17/06/2020.
 */
open class BaseViewModel : ViewModel() {

    /**
     * Executes a repository request as an async task, observing its response in the main thread.
     *
     * @param Result
     * @param observable
     * @return
     */
    protected fun <Result> getAsyncData(observable: Observable<BaseData<Result>>): Observable<BaseData<Result>> {

        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { }
    }

}