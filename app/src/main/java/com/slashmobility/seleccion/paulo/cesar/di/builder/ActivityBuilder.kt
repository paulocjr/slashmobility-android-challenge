package com.slashmobility.seleccion.paulo.cesar.di.builder

import com.slashmobility.seleccion.paulo.cesar.di.scope.Activity
import com.slashmobility.seleccion.paulo.cesar.view.list.ListActivity
import com.slashmobility.seleccion.paulo.cesar.view.main.MainActivity
import com.slashmobility.seleccion.paulo.cesar.view.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by pcamilo on 17/06/2020.
 */
@Module
abstract class ActivityBuilder {

    @Activity
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @Activity
    @ContributesAndroidInjector
    abstract fun bindListActivity(): ListActivity

    @Activity
    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity
}