package com.slashmobility.seleccion.paulo.cesar.view.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import javax.inject.Inject

/**
 * Created by pcamilo on 17/06/2020.
 */
abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> (@LayoutRes private val layoutResId: Int) : AppCompatActivity(){

    private lateinit var mDataBinding: T
    private lateinit var mViewModel: V

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mDataBinding = DataBindingUtil.setContentView(this, layoutResId)

        getViewModelClass().let {
            mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(getViewModelClass())
            getBinding().lifecycleOwner = this

            getBinding().apply {
                setVariable(BR.viewModel, mViewModel)
            }
        }

        initBinding()
    }

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    fun getBinding(): T = mDataBinding

    /**
     * Returns the current view model
     *
     * @return the T is a generic type
     */
    fun getViewModel(): V = mViewModel

    /**
     * Initialize the binding for layouts in Activities or Fragments
     */
    protected abstract fun initBinding()
    protected abstract fun getViewModelClass(): Class<V>

}