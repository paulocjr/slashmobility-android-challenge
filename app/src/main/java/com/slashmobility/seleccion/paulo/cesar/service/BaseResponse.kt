package com.slashmobility.seleccion.paulo.cesar.service

/**
 * Created by pcamilo on 17/06/2020.
 */
class BaseResponse<T>(val success: Boolean, val data: T?)