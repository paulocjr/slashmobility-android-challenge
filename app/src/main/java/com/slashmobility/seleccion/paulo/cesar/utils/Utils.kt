package com.slashmobility.seleccion.paulo.cesar.utils

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.EditText

/**
 * Created by pcamilo on 17/06/2020.
 */
object Utils {

    /**
     * Blinking animation in View
     */
    fun blinkingAnimation(view: View) {
        val animation = AlphaAnimation(0.3f, 1.0f)
        animation.duration = 500
        animation.startOffset = 20
        animation.repeatMode = Animation.REVERSE
        animation.repeatCount = Animation.INFINITE
        view.startAnimation(animation)
    }

    /**
     * Check if field is empty
     */
    fun fieldIsEmpty(view: EditText) : Boolean {
        return view.text.isNullOrEmpty()
    }
}