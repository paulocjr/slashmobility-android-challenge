package com.slashmobility.seleccion.paulo.cesar.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.slashmobility.seleccion.paulo.cesar.di.ViewModelKey
import com.slashmobility.seleccion.paulo.cesar.viewmodel.ViewModelFactory
import com.slashmobility.seleccion.paulo.cesar.viewmodel.list.ListViewModel
import com.slashmobility.seleccion.paulo.cesar.viewmodel.main.MainViewModel
import com.slashmobility.seleccion.paulo.cesar.viewmodel.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by pcamilo on 17/06/2020.
 */
@Module
abstract class ViewModelBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(splashViewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindListViewModel(listViewModel: ListViewModel): ViewModel

    // ViewModel Factory
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}